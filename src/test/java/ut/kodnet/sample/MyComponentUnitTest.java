package ut.kodnet.sample;

import org.junit.Test;
import kodnet.sample.api.MyPluginComponent;
import kodnet.sample.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}