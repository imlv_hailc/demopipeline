package kodnet.sample.api;

public interface MyPluginComponent
{
    String getName();
}